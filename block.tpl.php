<div id="updates" class="boxed">
<div class="<?php print "block block-$block->module" ?>" id="<?php print "block-$block->module-$block->delta"; ?>">
  <?php if ($block->region == 'left') { ?><div class="title"><h2><?php print $block->subject ?></h2></div><?php } ?>
  <?php if ($block->region == 'right'){ ?><div id="sponsors" class="boxed"><div class="title"><h2><?php print $block->subject; ?></h2></div></div><?php } ?>
  <div class="content"><?php print $block->content ?></div>
</div>
</div>
