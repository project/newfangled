<div class="post">	
	<div <?php if ($comment->status == COMMENT_NOT_PUBLISHED) print ' comment-unpublished'; ?>">
		  <h2 class="title"><span><?php print $title ?></h2> <div class="submitted"><?php print $submitted ?></div><?php if ($comment->new) : ?><div class="new"> *<?php print $new ?></div><?php endif; ?>	
</span>
	  <?php if ($picture) : ?>
				<div class="picture">  <?php print $picture ?>   </div>
			  <?php endif; ?>			
	<div class="comment"><?php print $content ?>
		  <!-- BEGIN: links -->
		 <div class="meta">&raquo;<p> <?php print $links ?></p></div></div>
		  <!-- END: links -->
	</div>
</div>