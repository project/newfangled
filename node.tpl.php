<div class="post">
	<div class="node"<?php if ($sticky) { print " sticky"; } ?><?php if (!$status) { print " node-unpublished"; } ?>>
	  <?php if ($page == 0): ?>
		<h2 class="title"><span><a href="<?php print $node_url ?>"><?php print $title ?></a></span></h2>
	  <?php endif; ?>
		<h3 class="date"><span class="taxonomy">Tags: <?php print $terms ?></span></h3>
		 <div class="submitted"><?php print $submitted ?></div>
			  <?php if ($picture) : ?>
				<div class="picture">  <?php print $picture ?>   </div>
			  <?php endif; ?>
		<div class="story"><?php print $content ?></div>
		<?php if ($links): ?>
		<div class="meta"><p>
		<?php print $links ?></p></div>
		<?php endif; ?>
	</div>
</div>