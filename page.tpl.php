<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--
Design by Free CSS Templates
http://www.freecsstemplates.org
Released for free under a Creative Commons Attribution 2.5 License

Title      : Newfangled
Version    : 1.0
Released   : 20070331
Description: A three-column fixed-width template ideal for 1024x768 pixel resolution. Suitable for blogs and small business websites.

-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title><?php print $head_title ?></title>
  <meta http-equiv="Content-Style-Type" content="text/css" />
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
</head>
<body>
<div id="header">
    <div id="sitename">
      <?php if ($logo) { ?><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" id="logo" alt="<?php print t('Home') ?>" /></a><?php } ?>
      <?php if ($site_name) { ?><h1><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></h1><?php } ?>
      <?php if ($site_slogan) { ?><h2><?php print $site_slogan ?></h2><?php } ?>
    </div>
	<div id="menu">
		<?php print theme('links', $primary_links, array('class' => 'links', 'id' => 'navlist')) ?>
	</div>
</div>
<div id="content">
	<div id="sidebar">
			<?php if ($sidebar_left != ""): ?>
			  <?php print $sidebar_left ?>
			<?php endif; ?>
		<!-- end #login -->
	</div>
	<!-- end #sidebar -->
	<div id="main">
		      <?php if ($mission != ""): ?>
				 <div id="mission"><?php print $mission ?></div>
			  <?php endif; ?>
          <?php print $breadcrumb ?>
        <?php if ($title != ""): ?>
          <div class="pagetitle"><?php print $title ?></div>

          <?php if ($tabs != ""): ?>
            <div class="tabs"><?php print $tabs ?></div>
          <?php endif; ?>

        <?php endif; ?>

        <?php if ($help != ""): ?>
            <div id="help"><?php print $help ?></div>
        <?php endif; ?>

        <?php if ($messages != ""): ?>
          <?php print $messages ?>
        <?php endif; ?>

		  <!-- start main content -->
		  <?php print $content; ?>
		  <?php print $feed_icons; ?>
		  <!-- end main content -->
		<!-- end #welcome -->

	</div>
	<!-- end #main -->
	<div id="sidebar2">
		    <?php if ($sidebar_right != ""): ?>
      <?php print $sidebar_right ?>
		<?php endif; ?>
	</div>
</div>
<!-- end #sidebar2 -->
<!-- end #content -->
<div id="footer">
<?php if ($footer_message) : ?>
    <p><?php print $footer_message;?></p>
<?php endif; ?>
<?php print $closure;?>
    <br/><br/> <br/>
	<p id="legal">Inspired by <a href="http://freecsstemplates.org">Newfangled</a> Re-Designed by <a href="http://mydrupal.com">My Drupal</a>.
	Sponsored by: <a href="http://itdiscover.com">IT Discover</a> | <a href="http://techjobs.co.in">Tech Jobs</a> | <a href="http://www.domainsellingtips.com/increase-traffic-tips">Traffic Tips</a></p>
 
</div>
</body>
</html>
